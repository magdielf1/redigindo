import React, { Component } from 'react';
import Navbar from './components/navbar';
import Footer from './components/footer';

class App extends Component {
	render() {
		return (
			<React.Fragment>
				<Navbar/>
				{this.props.children}
				<Footer/>
			</React.Fragment>
		);
	}
}

export default App;

	/*
	import React, { Component } from 'react';
	import Wall from './componentes/Header';
	import Side from './componentes/Side';
	import Rigth from './componentes/Rigth';
	import StatusPost from './componentes/StatusPost';
	import Post from './componentes/Post';
	import Lateral from './componentes/Lateral';

	class App extends Component {
		render() {
			return (
				<div className="App ">
					<Wall />
					<Side />
					<div className="content-posts active " id="posts">
						<div className="primary-content">
							<div
								id="posts-container"
								className="container-fluid container-posts">
								<div className="card-post">
									<StatusPost />
								</div>
								<div>
									<Post />
									<Post />
									<Post />
								</div>
							</div>
						</div>
						<Lateral />
					</div>
				</div>
			);
		}
	}

	export default App;
	*/


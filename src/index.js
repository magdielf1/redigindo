import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import firebaseApp from './utilidade/firebase/config';
import App from './App';
import NotFound from './components/not-found';
import Contact from './components/contact';
import EditProfileCommom from './components/edit-profile/commom';
import EditProfileBasic from './components/edit-profile/basic';
import EditProfileInterests from './components/edit-profile/interests';
import EditProfilePassword from './components/edit-profile/password';
import EditProfileSettings from './components/edit-profile/settings';
import EditProfileWorkEdu from './components/edit-profile/work-edu';
import Faq from './components/faq';
import Index from './components/index/index';
import NewsFeedCommom from './components/newsfeed/commom';
import FriendsNewsFeed from './components/newsfeed/friends';
import ImagesNewsFeed from './components/newsfeed/images';
import MessagesNewsFeed from './components/newsfeed/messages';
import NewsFeed from './components/newsfeed/newsfeed';
import PeopleNearbyNewsFeed from './components/newsfeed/people-nearby';
import VideosNewsFeed from './components/newsfeed/videos';
import TimelineCommom from './components/timeline/commom';
import TimelineAbout from './components/timeline/about';
import TimelineAlbum from './components/timeline/album';
import TimelineFriends from './components/timeline/friends';
import Timeline from './components/timeline/timeline';
import TeacherProfileCommom from './components/teacher-profile/commom';
import TeacherProfileMain from './components/teacher-profile/main';
import TeacherProfileCorrection from './components/teacher-profile/correction';
import Login from './components/index/login';
import Register from './components/index/register';

import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducer from './utilidade/reducers';

const store = createStore(reducer);

ReactDOM.render(
	<Provider store={store}>
		<Router history={hashHistory}>
			<Route path="/" component={App}>
				<Route component={Index}>
					<Route path="/register" component={Register} />
					<IndexRoute component={Login} />
					<Route path="/login" component={Login} />
				</Route>
				<Route
					path="/contact"
					component={Contact}
					firebase={firebaseApp}
					history={hashHistory}
				/>
				<Route
					path="/faq"
					component={Faq}
					firebase={firebaseApp}
					history={hashHistory}
				/>
				<Route
					path="/teacher"
					component={TeacherProfileMain}
					firebase={firebaseApp}
					history={hashHistory}
				/>
				<Route
					path="/teacher-correction"
					component={TeacherProfileCorrection}
					firebase={firebaseApp}
					history={hashHistory}
				/>
				<Route component={EditProfileCommom}>
					<Route
						path="/edit-profile-basic"
						component={EditProfileBasic}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/edit-profile-interests"
						component={EditProfileInterests}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/edit-profile-password"
						component={EditProfilePassword}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/edit-profile-settings"
						component={EditProfileSettings}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/edit-profile-work-edu"
						component={EditProfileWorkEdu}
						firebase={firebaseApp}
						history={hashHistory}
					/>
				</Route>
				{/* <Route component={TeacherProfileCommom}>

			</Route> */}
				<Route component={TimelineCommom}>
					<Route
						path="/timeline"
						component={Timeline}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/timeline-about"
						component={TimelineAbout}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/timeline-album"
						component={TimelineAlbum}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/timeline-friends"
						component={TimelineFriends}
						firebase={firebaseApp}
						history={hashHistory}
					/>
				</Route>
			</Route>
			<Route component={App}>
				<Route component={NewsFeedCommom}>
					<Route
						path="/newsfeed"
						component={NewsFeed}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/newsfeed-friends"
						component={FriendsNewsFeed}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/newsfeed-images"
						component={ImagesNewsFeed}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/newsfeed-messages"
						component={MessagesNewsFeed}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/redacoes"
						component={PeopleNearbyNewsFeed}
						firebase={firebaseApp}
						history={hashHistory}
					/>
					<Route
						path="/newsfeed-videos"
						component={VideosNewsFeed}
						firebase={firebaseApp}
						history={hashHistory}
					/>
				</Route>
			</Route>
			<Route path="*" exact={true} component={NotFound} />
		</Router>
	</Provider>,
	document.getElementById('root')
);
registerServiceWorker();

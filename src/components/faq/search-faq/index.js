import React, { Component } from 'react';

class SearchFaq extends Component {
    render() {
        return (
            <div class="page-title-section faq">
                <div class="container">
                    <div class="headline">
                        <h1 class="title">How Can We Help You?</h1>
                    </div>
                    <div class="page-search">
                        <form class="search-form">
                            <div class="form-group">
                                <label for="search_content" class="screen-reader-text">Search Content!</label>
                                <input id="search_content" type="text" class="form-control" value="" placeholder="Search what you want to find..." />
                            </div>
                            <button type="submit" class="btn btn-primary" value=""><i class="icon ion-ios-search-strong"></i></button>
                        </form>
                    </div>
                </div>
            </div> 
        );
    }
}

export default SearchFaq;


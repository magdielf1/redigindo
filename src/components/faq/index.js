import React, { Component } from 'react'
import SearchFaq from './search-faq';
import NavbarFaq from './navbar-faq';
import HelpFaq from './help-faq';
import GeneralQuestionsFaq from './general-questions-faq';
import AccountFaq from './account-faq';
import PrivacyFaq from './privacy-faq';
import OtherFaq from './other-faq';

class Faq extends Component {
    render() {
        return (
            <React.Fragment>
                <SearchFaq/>
                <div id="page-contents">
                    <div class="container ">

                        <NavbarFaq/>

                        <div class="row">
                            <div class="col-sm-9">
                                <div class="tab-content faq-content">

                                    <GeneralQuestionsFaq/>

                                    <AccountFaq/>

                                    <PrivacyFaq/>                                    

                                    <OtherFaq/>                                  

                                </div>
                            </div>
                            <HelpFaq/>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Faq;

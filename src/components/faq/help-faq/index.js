import React, { Component } from 'react';

class HelpFaq extends Component {
    render() {
        return (
            <div class="col-sm-3">
                <div class="faq_contact">
                    <div class="faq-headline">
                        <h3 class="item-title grey"><i class="icon ion-ios-help-outline"></i>Still Confused?</h3>
                        <p>Contact us if you still have any question.</p>
                    </div>
                    <div class="reach-out">
                        <div class="method by-forum"><a href="/"><i class="icon ion-chatboxes"></i>Ask in Forum</a></div>
                        <div class="method by-ticket"><a href="/"><i class="icon ion-help-circled"></i>Send Ticket</a></div>
                        <div class="method by-email"><a href="/"><i class="icon ion-email"></i>Email us</a></div>
                        <div class="method by-phone"><a href="/"><i class="icon ion-android-call"></i>Call us</a></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default HelpFaq;


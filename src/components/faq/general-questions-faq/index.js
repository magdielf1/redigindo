import React, { Component } from 'react';

class GeneralQuestionsFaq extends Component {
    render() {
        return (
            <div class="tab-pane active" id="faq_cat_1">
                <div class="faq-headline">
                    <h3 class="item-title grey"><i class="icon ion-ios-information-outline"></i>General Questions</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias.</p>
                </div>
                <div class="panel-group" id="faqAccordion-1">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion-1" data-target="#question1">
                            <h4 class="panel-title"><a href="/" class="ing">Q: What is Lorem Ipsum?</a></h4>
                        </div>
                        <div id="question1" class="panel-collapse collapse" style={{ height: 0 }}>
                            <div class="panel-body">
                                <h5><span class="label label-primary">Answer</span></h5>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            </div>
                        </div>
                    </div>{ /*  .panel */}
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion-1" data-target="#question2">
                            <h4 class="panel-title"><a href="/" class="ing">Q: Why do we use it?</a></h4>
                        </div>
                        <div id="question2" class="panel-collapse collapse" style={{ height: 0 }}>
                            <div class="panel-body">
                                <h5><span class="label label-primary">Answer</span></h5>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                            </div>
                        </div>
                    </div>{ /*  .panel */}
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion-1" data-target="#question3">
                            <h4 class="panel-title"><a href="/" class="ing">Q: Where does it come from?</a></h4>
                        </div>
                        <div id="question3" class="panel-collapse collapse" style={{ height: 0 }}>
                            <div class="panel-body">
                                <h5><span class="label label-primary">Answer</span></h5>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32</p>
                            </div>
                        </div>
                    </div>{ /*  .panel */}
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion-1" data-target="#question4">
                            <h4 class="panel-title"><a href="/" class="ing">Q: Where can I get some?</a></h4>
                        </div>
                        <div id="question4" class="panel-collapse collapse" style={{ height: 0 }}>
                            <div class="panel-body">
                                <h5><span class="label label-primary">Answer</span></h5>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                            </div>
                        </div>
                    </div>{ /*  .panel */}
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion-1" data-target="#question5">
                            <h4 class="panel-title"><a href="/" class="ing">Q: Who uses lorem ipsum?</a></h4>
                        </div>
                        <div id="question5" class="panel-collapse collapse" style={{ height: 0 }}>
                            <div class="panel-body">
                                <h5><span class="label label-primary">Answer</span></h5>
                                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                            </div>
                        </div>
                    </div>{ /*  .panel */}
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion-1" data-target="#question6">
                            <h4 class="panel-title"><a href="/" class="ing">Q: When they use it?</a></h4>
                        </div>
                        <div id="question6" class="panel-collapse collapse" style={{ height: 0 }}>
                            <div class="panel-body">
                                <h5><span class="label label-primary">Answer</span></h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                        </div>
                    </div>{ /*  .panel */}
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion-1" data-target="#question7">
                            <h4 class="panel-title"><a href="/" class="ing">Q: Who can see this?</a></h4>
                        </div>
                        <div id="question7" class="panel-collapse collapse" style={{ height: 0 }}>
                            <div class="panel-body">
                                <h5><span class="label label-primary">Answer</span></h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                        </div>
                    </div>{ /*  .panel */}
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion-1" data-target="#question8">
                            <h4 class="panel-title"><a href="/" class="ing">Q: Why people love this?</a></h4>
                        </div>
                        <div id="question8" class="panel-collapse collapse" style={{ height: 0 }}>
                            <div class="panel-body">
                                <h5><span class="label label-primary">Answer</span></h5>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                            </div>
                        </div>
                    </div>{ /*  .panel */}
                </div>{ /*  .panel-group */}
            </div>
        );
    }
}

export default GeneralQuestionsFaq;


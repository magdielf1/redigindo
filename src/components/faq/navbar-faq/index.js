import React, { Component } from 'react';

class NavbarFaq extends Component {
    render() {
        return (
            <ul class="nav nav-tabs faq-cat-list">
                <li class="active"><a href="#faq_cat_1" data-toggle="tab"><i class="icon ion-ios-information-outline"></i>General</a></li>
                <li><a href="#faq_cat_2" data-toggle="tab"><i class="icon ion-ios-person"></i>Account</a></li>
                <li><a href="#faq_cat_3" data-toggle="tab"><i class="icon ion-eye-disabled"></i>Privacy</a></li>
                <li><a href="#faq_cat_4" data-toggle="tab"><i class="icon ion-ios-plus-outline"></i>Other</a></li>
            </ul>
        );
    }
}

export default NavbarFaq;


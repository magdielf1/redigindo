import React, { Component } from 'react';

class PrivacyFaq extends Component {
    render() {
        return (
            <div class="tab-pane" id="faq_cat_3">
                <div class="faq-headline">
                    <h3 class="item-title grey"><i class="icon ion-eye-disabled"></i>Privacy FAQ</h3>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi</p>
                </div>
                <div class="panel-group" id="faqAccordion-3">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion-3" data-target="#question12">
                            <h4 class="panel-title"><a href="/" class="ing">Q: Why Privacy is Important?</a></h4>
                        </div>
                        <div id="question12" class="panel-collapse collapse" style={{ height: 0 }}>
                            <div class="panel-body">
                                <h5><span class="label label-primary">Answer</span></h5>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur</p>
                            </div>
                        </div>
                    </div>{ /*  .panel */}
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion-3" data-target="#question13">
                            <h4 class="panel-title"><a href="/" class="ing">Q: Why Should I care?</a></h4>
                        </div>
                        <div id="question13" class="panel-collapse collapse" style={{ height: 0 }}>
                            <div class="panel-body">
                                <h5><span class="label label-primary">Answer</span></h5>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            </div>
                        </div>
                    </div>{ /*  .panel */}
                </div>{ /* .panel-group*/}
            </div>
        );
    }
}

export default PrivacyFaq;





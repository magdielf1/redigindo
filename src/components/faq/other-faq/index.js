import React, { Component } from 'react';

class OtherFaq extends Component {
    render() {
        return (
            <div class="tab-pane" id="faq_cat_4">
                <div class="faq-headline">
                    <h3 class="item-title grey"><i class="icon ion-ios-plus-outline"></i>Other FAQ</h3>
                    <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod</p>
                </div>
                <div class="panel-group" id="faqAccordion-4">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion-4" data-target="#question14">
                            <h4 class="panel-title"><a href="/" class="ing">Q: What are Other Questions?</a></h4>
                        </div>
                        <div id="question14" class="panel-collapse collapse" style={{ height: 0 }}>
                            <div class="panel-body">
                                <h5><span class="label label-primary">Answer</span></h5>
                                <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus</p>
                            </div>
                        </div>
                    </div>{ /*  .panel */}
                </div>{ /* .panel-group*/}
            </div>
        );
    }
}

export default OtherFaq;

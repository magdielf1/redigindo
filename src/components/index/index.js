import React, { Component } from 'react';

class Index extends Component {
	constructor(props) {
		super(props);
		this.state = {
			current: ''
		};
	}

	render() {
		return (
			<React.Fragment>
				<section id="banner">
					<div class="container">
						{React.cloneElement(this.props.children, {})}
						<svg class="arrows hidden-xs hidden-sm">
							<path class="a1" d="M0 0 L30 32 L60 0" />
							<path class="a2" d="M0 20 L30 52 L60 20" />
							<path class="a3" d="M0 40 L30 72 L60 40" />
						</svg>
					</div>
				</section>
				<section id="features">
					<div class="container wrapper">
						<h1 class="section-title slideDown">explore</h1>
						<div class="row slideUp">
							<div class="feature-item col-md-2 col-sm-6 col-xs-6 col-md-offset-2">
								<div class="feature-icon-one" />
								<h3>Conheça Outros Estudantes</h3>
							</div>
							<div class="feature-item col-md-2 col-sm-6 col-xs-6">
								<div class="feature-icon-two" />
								<h3>Pratique Sua Redação</h3>
							</div>
							<div class="feature-item col-md-2 col-sm-6 col-xs-6">
								<div class="feature-icon-tree" />
								<h3>Professores Reais e Auditados</h3>
							</div>
							<div class="feature-item col-md-2 col-sm-6 col-xs-6">
								<div class="feature-icon-four" />
								<h3>Desafie Outros Alunos</h3>
							</div>
						</div>
						<h2 class="sub-title">Alunos de todo o Brasil</h2>
						<div id="incremental-counter" data-value="101242" />
						<p>Alunos atualmente participantes da rede</p>
					</div>
				</section>

				<section id="site-facts">
					<div class="container wrapper">
						<div class="circle">
							<ul class="facts-list">
								<li>
									<div class="fact-icon">
										<i class="icon ion-ios-people-outline" />
									</div>
									<h3 class="text-white">1,01,242</h3>
									<p>Estudantes Cadastrados</p>
								</li>
								<li>
									<div class="fact-icon">
										<i class="icon ion-images" />
									</div>
									<h3 class="text-white">21,01,242</h3>
									<p>Redações Corrigidas</p>
								</li>
								<li>
									<div class="fact-icon">
										<i class="icon ion-checkmark-round" />
									</div>
									<h3 class="text-white">1,242</h3>
									<p>Professores Auditados</p>
								</li>
							</ul>
						</div>
					</div>
				</section>

				<section id="live-feed">
					<div class="container wrapper">
						<h3 class="section-title slideDown">TOP RANK PROFESSORES</h3>
						<span>Professores em destaque da semana</span>
						<ul class="online-users list-inline slideUp">
							<li>
								<a href="/" title="Alexis Clark">
									<img
										src="http://placehold.it/300x300"
										alt=""
										class="img-responsive profile-photo"
									/>
									<span class="online-dot" />
								</a>
							</li>
							<li>
								<a href="/" title="James Carter">
									<img
										src="http://placehold.it/300x300"
										alt=""
										class="img-responsive profile-photo"
									/>
									<span class="online-dot" />
								</a>
							</li>
							<li>
								<a href="/" title="Robert Cook">
									<img
										src="http://placehold.it/300x300"
										alt=""
										class="img-responsive profile-photo"
									/>
									<span class="online-dot" />
								</a>
							</li>
							<li>
								<a href="/" title="Richard Bell">
									<img
										src="http://placehold.it/300x300"
										alt=""
										class="img-responsive profile-photo"
									/>
									<span class="online-dot" />
								</a>
							</li>
							<li>
								<a href="/" title="Anna Young">
									<img
										src="http://placehold.it/300x300"
										alt=""
										class="img-responsive profile-photo"
									/>
									<span class="online-dot" />
								</a>
							</li>
							<li>
								<a href="/" title="Julia Cox">
									<img
										src="http://placehold.it/300x300"
										alt=""
										class="img-responsive profile-photo"
									/>
									<span class="online-dot" />
								</a>
							</li>
						</ul>
					</div>
				</section>
			</React.Fragment>
		);
	}
}

export default Index;

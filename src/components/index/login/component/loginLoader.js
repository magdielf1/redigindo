import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, hashHistory } from 'react-router';
import ReactLoading from 'react-loading';
import FadeIn from 'react-fade-in';
import firebaseApp from '../../../../utilidade/firebase/config';
class loginLoader extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: '',
			thisLoading: false
		};
	}
	loginUser = () => {
		let _this = this;
		const email = this.state.email,
			password = this.state.password;
		firebaseApp
			.auth()
			.signInWithEmailAndPassword(email, password)
			.then(result => {})
			.catch(error => {
				let errorCode = error.code;
				console.log(errorCode);
			});
	};
	logout() {
		firebaseApp
			.auth()
			.signOut()
			.then(
				function() {
					window.location.reload();
				},
				function(error) {
					// An error happened.
				}
			);
	}

	componentWillMount() {}
	render() {
		const { profile, userStatus } = this.props.state;
		return (
			<React.Fragment>
				{profile.userName ? (
					<React.Fragment>
						<FadeIn>
							<div class="profile-card">
								<img
									src="http://placehold.it/300x300"
									alt="user"
									class="profile-photo"
								/>
								<h5>
									<span>
										<Link to="newsfeed" class="text-profile">
											{' '}
											{profile.userName}
										</Link>(<a
											href="#"
											onClick={() => this.logout()}
											class="text-logout"
										>
											<span>sair</span>
										</a>)
									</span>
								</h5>
								<a href="/" class="text-white">
									<span>Rank #123</span>
								</a>
							</div>
						</FadeIn>
					</React.Fragment>
				) : (
					<React.Fragment>
						<FadeIn>
							<p class="signup-text">
								Informe os dados para ter acesso a plataforma
							</p>

							<form action="#">
								<fieldset class="form-group">
									<input
										type="email"
										class="form-control"
										id="example-email"
										placeholder="E-mail"
										value={this.state.email}
										onChange={e => this.setState({ email: e.target.value })}
									/>
								</fieldset>
								<fieldset class="form-group">
									<input
										type="password"
										class="form-control"
										id="example-password"
										placeholder="Senha"
										value={this.state.password}
										onChange={e => this.setState({ password: e.target.value })}
									/>
								</fieldset>
							</form>

							<button class="btn-secondary" onClick={() => this.loginUser()}>
								Acessar
							</button>
						</FadeIn>
					</React.Fragment>
				)}
			</React.Fragment>
		);
	}
}

const mapStateToProps = function(state) {
	return { state: state };
};

export default connect(mapStateToProps)(loginLoader);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, hashHistory } from 'react-router';
import ReactLoading from 'react-loading';
import LoginLoader from './component/loginLoader';

import firebaseApp from '../../../utilidade/firebase/config';

class Login extends Component {
	componentWillMount() {}
	render() {
		const { profile, userStatus } = this.props.state;
		return (
			<React.Fragment>
				<div class="sign-up-form">
					<h2 class="text-white">Acessar Rede</h2>
					<div class="line-divider" />
					<div class="form-wrapper">
						{userStatus.loading ? (
							<div
								style={{
									position: 'relative',
									left: '50%',
									top: '-20px',
									width: 10,
									height: 10
								}}
							>
								<ReactLoading type="spin" width="20px" color="#9ddffb" />
							</div>
						) : (
							<LoginLoader />
						)}
					</div>
					<Link to="/register">Ainda não possui uma conta?</Link>
					<img class="form-shadow" src="images/bottom-shadow.png" alt="" />
				</div>
			</React.Fragment>
		);
	}
}

const mapStateToProps = function(state) {
	return { state: state };
};

export default connect(mapStateToProps)(Login);

import React, { Component } from 'react';
import FadeIn from 'react-fade-in';
import firebaseApp from '../../../utilidade/firebase/config';
import { Link, hashHistory } from 'react-router';

class Register extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			username: '',
			password: '',
			thisLoading: false
		};
	}
	updateUser = user => {
		let _this = this;
		let updateDisplay = firebaseApp.auth().currentUser;
		updateDisplay
			.updateProfile({
				displayName: user
			})
			.then(function(user) {
				hashHistory.push('/newsfeed');
			})
			.catch(function(error) {
				console.log(error);
			});
	};

	registerUser = () => {
		let _this = this;
		const email = this.state.email,
			password = this.state.password,
			username = this.state.username;
		firebaseApp
			.auth()
			.createUserWithEmailAndPassword(email, password)
			.then(user => {
				_this.updateUser(_this.state.username);
			})
			.catch(error => {
				let errorCode = error.code;
				console.log(errorCode);
			});
	};

	render() {
		return (
			<React.Fragment>
				<div class="sign-up-form">
					<FadeIn>
						<h2 class="text-white">Cadastre-se</h2>
						<div class="line-divider" />
						<div class="form-wrapper">
							<p class="signup-text">
								Cadastre-se para participar da plataforma.
							</p>
							<form action="#">
								<fieldset class="form-group">
									<input
										type="text"
										class="form-control"
										id="example-name"
										placeholder="Nome"
										value={this.state.username}
										onChange={e => this.setState({ username: e.target.value })}
									/>
								</fieldset>
								<fieldset class="form-group">
									<input
										type="email"
										class="form-control"
										id="example-email"
										placeholder="E-mail"
										value={this.state.email}
										onChange={e => this.setState({ email: e.target.value })}
									/>
								</fieldset>
								<fieldset class="form-group">
									<input
										type="password"
										class="form-control"
										id="example-password"
										placeholder="Senha"
										value={this.state.password}
										onChange={e => this.setState({ password: e.target.value })}
									/>
								</fieldset>
							</form>
							<p>Ao se registrar você concorda com nossos termos.</p>
							<button class="btn-secondary" onClick={() => this.registerUser()}>
								Registrar
							</button>
						</div>
						<Link to="/login">Já possui uma conta?</Link>
						<img class="form-shadow" src="images/bottom-shadow.png" alt="" />
					</FadeIn>
				</div>
			</React.Fragment>
		);
	}
}

export default Register;

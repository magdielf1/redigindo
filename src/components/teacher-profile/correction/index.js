import React, { Component } from 'react';

class TeacherProfileCorrection extends Component {
    render() {
        return (
            <div className="container correction">
            <h4 className="grey"><i className="icon ion-android-checkmark-circle"></i> Área de correção</h4>
                <div className="col-md-8">
                    <div className="row">
                        <div className="editorial-correction">
                            <img src="./images/redaction.png" />
                        </div>
                    </div>
                    <div className="row">
                        <p className="custom-label title"><strong>Correção conteúdo</strong></p>
                        <div className="table-responsive">
                            <table className="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Competência</th>
                                        <th>Conceito</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><b>I</b> Modalidade escrita formal em Língua Portuguesa</td>
                                        <td>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="point0" name="competence1" className="custom-control-input" />
                                                <label className="custom-control-label" for="point0">0</label>
                                                <input type="radio" id="point1" name="competence1" className="custom-control-input" />
                                                <label className="custom-control-label" for="point1">1</label>
                                                <input type="radio" id="point2" name="competence1" className="custom-control-input" />
                                                <label className="custom-control-label" for="point2">2</label>
                                                <input type="radio" id="point3" name="competence1" className="custom-control-input" />
                                                <label className="custom-control-label" for="point3">3</label>
                                                <input type="radio" id="point4" name="competence1" className="custom-control-input" />
                                                <label className="custom-control-label" for="point4">4</label>
                                                <input type="radio" id="point5" name="competence1" className="custom-control-input" />
                                                <label className="custom-control-label" for="point5">5</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>II</b> Compreensão da proposta de redação</td>
                                        <td>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="point0" name="competence2" className="custom-control-input" />
                                                <label className="custom-control-label" for="point0">0</label>
                                                <input type="radio" id="point1" name="competence2" className="custom-control-input" />
                                                <label className="custom-control-label" for="point1">1</label>
                                                <input type="radio" id="point2" name="competence2" className="custom-control-input" />
                                                <label className="custom-control-label" for="point2">2</label>
                                                <input type="radio" id="point3" name="competence2" className="custom-control-input" />
                                                <label className="custom-control-label" for="point3">3</label>
                                                <input type="radio" id="point4" name="competence2" className="custom-control-input" />
                                                <label className="custom-control-label" for="point4">4</label>
                                                <input type="radio" id="point5" name="competence2" className="custom-control-input" />
                                                <label className="custom-control-label" for="point5">5</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>III</b> Seleção/Organização de argumentos</td>
                                        <td>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="point0" name="competence3" className="custom-control-input" />
                                                <label className="custom-control-label" for="point0">0</label>
                                                <input type="radio" id="point1" name="competence3" className="custom-control-input" />
                                                <label className="custom-control-label" for="point1">1</label>
                                                <input type="radio" id="point2" name="competence3" className="custom-control-input" />
                                                <label className="custom-control-label" for="point2">2</label>
                                                <input type="radio" id="point3" name="competence3" className="custom-control-input" />
                                                <label className="custom-control-label" for="point3">3</label>
                                                <input type="radio" id="point4" name="competence3" className="custom-control-input" />
                                                <label className="custom-control-label" for="point4">4</label>
                                                <input type="radio" id="point5" name="competence3" className="custom-control-input" />
                                                <label className="custom-control-label" for="point5">5</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>IV</b> Construção de argumentos</td>
                                        <td>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="point0" name="competence4" className="custom-control-input" />
                                                <label className="custom-control-label" for="point0">0</label>
                                                <input type="radio" id="point1" name="competence4" className="custom-control-input" />
                                                <label className="custom-control-label" for="point1">1</label>
                                                <input type="radio" id="point2" name="competence4" className="custom-control-input" />
                                                <label className="custom-control-label" for="point2">2</label>
                                                <input type="radio" id="point3" name="competence4" className="custom-control-input" />
                                                <label className="custom-control-label" for="point3">3</label>
                                                <input type="radio" id="point4" name="competence4" className="custom-control-input" />
                                                <label className="custom-control-label" for="point4">4</label>
                                                <input type="radio" id="point5" name="competence4" className="custom-control-input" />
                                                <label className="custom-control-label" for="point5">5</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>V</b> Proposta de intervenção</td>
                                        <td>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="point0" name="competence5" className="custom-control-input" />
                                                <label className="custom-control-label" for="point0">0</label>
                                                <input type="radio" id="point1" name="competence5" className="custom-control-input" />
                                                <label className="custom-control-label" for="point1">1</label>
                                                <input type="radio" id="point2" name="competence5" className="custom-control-input" />
                                                <label className="custom-control-label" for="point2">2</label>
                                                <input type="radio" id="point3" name="competence5" className="custom-control-input" />
                                                <label className="custom-control-label" for="point3">3</label>
                                                <input type="radio" id="point4" name="competence5" className="custom-control-input" />
                                                <label className="custom-control-label" for="point4">4</label>
                                                <input type="radio" id="point5" name="competence5" className="custom-control-input" />
                                                <label className="custom-control-label" for="point5">5</label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="row">
                        <p className="custom-label title"><strong>Problemas na competência <b>I</b></strong></p>
                        <div className="table-responsive">
                            <table className="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <th>4</th>
                                        <th>5</th>
                                        <th>6</th>
                                        <th>7</th>
                                        <th>8</th>
                                        <th>9</th>
                                        <th>10</th>
                                        <th>11</th>
                                        <th>12</th>
                                        <th>13</th>
                                        <th>14</th>
                                        <th>15</th>
                                        <th>16</th>
                                        <th>17</th>
                                        <th>18</th>
                                        <th>19</th>
                                        <th>20</th>
                                        <th>21</th>
                                        <th>22</th>
                                        <th>23</th>
                                        <th>24</th>
                                        <th>25</th>
                                        <th>26</th>
                                        <th>27</th>
                                        <th>28</th>
                                        <th>29</th>
                                        <th>30</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="checkbox" id="line1" name="line1" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line2" name="line2" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line3" name="line3" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line4" name="line4" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line5" name="line5" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line6" name="line6" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line7" name="line7" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line8" name="line8" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line9" name="line9" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line10" name="line10" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line11" name="line11" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line12" name="line12" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line13" name="line13" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line14" name="line14" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line15" name="line15" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line16" name="line16" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line17" name="line17" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line18" name="line18" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line19" name="line19" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line20" name="line20" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line21" name="line21" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line22" name="line22" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line23" name="line23" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line24" name="line24" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line25" name="line25" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line26" name="line26" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line27" name="line27" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line28" name="line28" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line29" name="line29" className="custom-control-input" />
                                        </td>
                                        <td>
                                            <input type="checkbox" id="line30" name="line30" className="custom-control-input" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="row">
                        <p className="custom-label title"><strong>Problemas na competência <b>V</b></strong></p>
                        <textarea></textarea>
                    </div>
                    <div className="row">
                        <p className="custom-label title"><strong>Outros comentários</strong></p>
                        <textarea></textarea>
                    </div>
                </div>
                <div className="col-md-4 correction">
                    <div className="row">
                        <p className="custom-label title"><strong>Situação</strong></p>
                        <div class="btn-group-vertical btn-block">
                            <button type="button" class="btn btn-default btn-block">Formas elementares</button>
                            <button type="button" class="btn btn-default btn-block">Texto insuficiente</button>
                            <button type="button" class="btn btn-default btn-block">Fuga ao tema</button>
                            <button type="button" class="btn btn-default btn-block">Não atendimento ao tipo textual</button>
                            <button type="button" class="btn btn-default btn-block">Parte desconectada</button>
                            <button type="button" class="btn btn-default btn-block">Cópia integral</button>
                        </div>
                        <hr/>
                        <button className="btn btn-success btn-rounded btn-block">Enviar</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default TeacherProfileCorrection;

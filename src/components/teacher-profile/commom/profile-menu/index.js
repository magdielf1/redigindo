import React, { Component } from 'react';
import { Link } from 'react-router/lib';

class ProfileMenu extends Component {

    componentWillMount(){
      
    }

    isActive(option) {
		let { pathname } = this.props.location;

        if(option === pathname){
            return true;
        }
        return false;
	}

    render() {
        return (
            <div className="col-md-3">
                <ul className="edit-menu">
                    <li className={this.isActive('/teacher') ? 'active' : ''}><i className="icon ion-ios-information-outline"></i><Link to="/teacher">Basic Information</Link></li>
                    <li className={this.isActive('/teacher-correction') ? 'active' : ''}><i className="icon ion-ios-star-half"></i><Link to="/teacher-correction">Correction area</Link></li>
                </ul>
            </div>
        );
    }
}

export default ProfileMenu;



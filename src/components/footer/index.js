import React, { Component } from 'react';

class Footer extends Component {
	render() {
		return (
			<footer id="footer">
				<div className="container">
					<div className="row">
						<div className="footer-wrapper">
							<div className="col-md-3 col-sm-3">
								<a href="">
									<img
										src="images/logo2.png"
										alt=""
										className="footer-logo"
										style={{ width: 180 }}
									/>
								</a>
								<ul className="list-inline social-icons">
									<li>
										<a href="/">
											<i className="icon ion-social-facebook" />
										</a>
									</li>
									<li>
										<a href="/">
											<i className="icon ion-social-twitter" />
										</a>
									</li>
									<li>
										<a href="/">
											<i className="icon ion-social-googleplus" />
										</a>
									</li>
									<li>
										<a href="/">
											<i className="icon ion-social-pinterest" />
										</a>
									</li>
									<li>
										<a href="/">
											<i className="icon ion-social-linkedin" />
										</a>
									</li>
								</ul>
							</div>
							<div className="col-md-2 col-sm-2">
								<h5>Para Alunos</h5>
								<ul className="footer-links">
									<li>
										<a href="">Cadastre-se</a>
									</li>
									<li>
										<a href="">Login</a>
									</li>
									<li>
										<a href="">Funcionalidades</a>
									</li>
									<li>
										<a href="">Pacotes</a>
									</li>
								</ul>
							</div>
							<div className="col-md-2 col-sm-2">
								<h5>Empresas</h5>
								<ul className="footer-links">
									<li>
										<a href="">Credenciamento</a>
									</li>
									<li>
										<a href="">Login</a>
									</li>
									<li>
										<a href="">Benfícios</a>
									</li>
									<li>
										<a href="">Funcionalidade</a>
									</li>
									<li>
										<a href="">Anuncie</a>
									</li>
								</ul>
							</div>
							<div className="col-md-2 col-sm-2">
								<h5>Sobre</h5>
								<ul className="footer-links">
									<li>
										<a href="">Sobre Nós</a>
									</li>
									<li>
										<a href="">Contato</a>
									</li>
									<li>
										<a href="">Política de Privacidade</a>
									</li>
									<li>
										<a href="">Termos de Uso</a>
									</li>
									<li>
										<a href="">FAQ</a>
									</li>
								</ul>
							</div>
							<div className="col-md-3 col-sm-3">
								<h5>Contact Us</h5>
								<ul className="contact">
									<li>
										<i className="icon ion-ios-telephone-outline" />CNPJ
									</li>
									<li>
										<i className="icon ion-ios-email-outline" />info@rendon.org
									</li>
									<li>
										<i className="icon ion-ios-location-outline" />Rua 2342, Sao
										Paulo
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div className="copyright">
					<p>RedON © 2016. Todos os direitos reservados.</p>
				</div>
			</footer>
		);
	}
}

export default Footer;

import React, { Component } from 'react';

class Proposta extends Component {
	render() {
		return (
			<div
				class="card"
				style={{
					width: '25rem',
					border: '1px #cecece solid',
					borderRadius: 4,
					overflow: 'hidden',
					margin: '0 0 10px 0'
				}}
			>
				<img
					class="card-img-top"
					src="./images/4.jpg"
					alt="Card image cap"
					style={{ width: 300 }}
				/>
				<p
					style={{
						background: '#484848d1',
						position: 'absolute',
						color: '#fffdfd',
						top: 0,
						borderRadius: 5,
						padding: 10
					}}
				>
					<sub>Tipo: {this.props.rank}</sub>
					<br />
					<sub>Pontos de Rank: {this.props.pontos}</sub>
					<br />
				</p>
				<div class="card-body">
					<h5 class="card-title" style={{ padding: 6 }}>
						{' '}
						Escravidão no mundo contemporâneo
					</h5>

					<p class="card-text" style={{ padding: 6 }}>
						Ninguém será mantido em escravidão ou servidão, a escravidão e o
						tráfico de escravos serão proibidos em todas as suas formas.
					</p>
					<p class="text-center">
						<a href="#" class="btn btn-primary">
							Escolher ({this.props.tipo})
						</a>
					</p>
				</div>
			</div>
		);
	}
}

export default Proposta;

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import FileUploadWithPreview from 'file-upload-with-preview';
import 'file-upload-with-preview/dist/file-upload-with-preview.min.css';
import Proposta from './component/propostas';

class PeopleNearbyNewsFeed extends Component {
	constructor(props) {
		super(props);
		this.state = {
			baseIMG: ''
		};
	}
	getBase64(file) {
		let _this = this;
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = function() {
			_this.setState({ baseIMG: reader.result });
		};
		reader.onerror = function(error) {
			console.log('Error: ', error);
		};
	}
	generateRedon() {
		const options = {
			onOpen: props => console.log('ABRIU'),
			onClose: props => console.log('FECHOU'),
			autoClose: 6000,
			closeButton: <div />,
			type: toast.TYPE.INFO,
			hideProgressBar: false,
			position: toast.POSITION.TOP_CENTER,
			pauseOnHover: false
		};
		toast('Estamos gerando sua redação, por favor aguarde!', options);
	}
	componentDidMount() {
		let _this = this;
		this.upload = new FileUploadWithPreview('uploadRedon');
		this.upload.imageSelected = function(event) {
			_this.getBase64(_this.upload.cachedFileArray[0]);
		};
	}
	render() {
		const { userStatus } = this.props.state;
		return (
			<div class="people-nearby clearfix">
				<div class="block-title">
					<h4 class="grey">
						<i class="icon ion-edit" style={{ color: '#4ea9d8' }} /> Escolha um
						Tema
					</h4>
					<div class="line" />
					<p>
						REDON está constantemente atualizando sua base de temas para
						garantir a maior performance da comunidade. Gere seu tema e
						desenvolva uma redação com base nesse tema.
					</p>

					<p style={{ marginBottom: 50 }}>
						<button
							class="btn btn-primary"
							onClick={() => this.generateRedon()}
						>
							Gerar Tema
						</button>
						<ToastContainer autoClose={2000} />
					</p>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<Proposta tipo="Com Tempo" rank="Ranqueada" pontos="5" />
					</div>
					<div class="col-sm-6">
						<Proposta tipo="Sem Tempo" rank="Não Ranqueada" pontos="0" />
					</div>
				</div>
				<div class="block-title">
					<h4 class="grey">
						<i class="icon ion-android-send" style={{ color: '#4ea9d8' }} />{' '}
						Envie a Redação
					</h4>
					<div class="line" />
					<p>
						Envie sua redação para um dos professores da rede, ao concluir a
						correção o professor escolhido devolverá o relatório completo do seu
						desempenho e comentário.
					</p>
					<div class="line" />
				</div>
				<div class="custom-file-container" data-upload-id="uploadRedon">
					<label class="custom-file-container__custom-file">
						<input
							type="file"
							class="custom-file-container__custom-file__custom-file-input"
							accept="*"
							multiple
						/>
						<label class="btn-redacao">
							<a
								href="javascript:void(0)"
								class="custom-file-container__image-clear"
								title="Clear Image"
							/>
						</label>
						<input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
						<span class="custom-file-container__custom-file__custom-file-control" />
					</label>
					<div
						class="custom-file-container__image-preview"
						style={{
							backgroundSize: 'contain',
							backgroundPosition: '0px 0px',
							backgroundColor: '#e8ebee'
						}}
					/>
				</div>
				<div class="block-title">
					<h4 class="grey">
						<i class="icon ion-ios-book" style={{ color: '#4ea9d8' }} /> Lista
						de Redações
					</h4>
					<div class="line" />
					<p>
						Liste e gerencie todas as redações que você já enviou para a rede.
					</p>
					<div class="line" />
					<table class="table">
						<thead>
							<tr>
								<th scope="col">ID</th>
								<th scope="col">Nota</th>
								<th scope="col">Tema</th>
								<th scope="col">Ações</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">1</th>
								<td>900</td>
								<td>Escravidão no mundo contemporâneo</td>
								<td>@mdo</td>
							</tr>
							<tr>
								<th scope="row">1</th>
								<td>300</td>
								<td>Esportes como ferramentas de inclusão social</td>
								<td>@mdo</td>
							</tr>
							<tr>
								<th scope="row">1</th>
								<td>800</td>
								<td>Privacidade na internet</td>
								<td>@mdo</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}

const mapStateToProps = function(state) {
	return { state: state };
};

const mapDispatchToProps = function(dispatch) {
	return {
		updateProfile: function(uid, picture, name) {
			return dispatch({ type: 'UPDATE_PROFILE', uid, picture, name });
		}
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(PeopleNearbyNewsFeed);

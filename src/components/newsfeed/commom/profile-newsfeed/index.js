import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import ReactLoading from 'react-loading';

class ProfileNewsFeed extends Component {
	render() {
		const { profile, userStatus } = this.props.state;
		return (
			<div class="col-md-3 static">
				<div class="profile-card">
					<img
						src="http://placehold.it/300x300"
						alt="user"
						class="profile-photo"
					/>
					<h5 style={{ width: 199 }}>
						<Link to="timeline" class="text-white">
							{userStatus.loading ? (
								<div
									style={{
										position: 'relative',
										left: '50%',
										top: '-50px',
										width: 10,
										height: 10
									}}
								>
									<ReactLoading type="spin" width="20px" color="#9ddffb" />
								</div>
							) : (
								<span>{profile.userName}</span>
							)}
						</Link>
					</h5>
					<a href="/" class="text-white">
						{userStatus.loading ? <div /> : <span>Rank #123</span>}
					</a>
				</div>
				<ul class="nav-news-feed">
					<li>
						<i class="icon ion-ios-paper" />
						<div>
							<Link to="/newsfeed">Minha Rede</Link>
						</div>
					</li>
					<li>
						<i class="icon ion-ios-book" style={{ color: '#ff0066' }} />
						<div>
							<Link to="/redacoes">Minhas Redações</Link>
						</div>
					</li>
					<li>
						<i class="icon ion-ios-people" />
						<div>
							<Link to="/newsfeed-friends">Meus Amigos</Link>
						</div>
					</li>
					<li>
						<i class="icon ion-chatboxes" />
						<div>
							<Link to="/newsfeed-messages">Mensagens</Link>
						</div>
					</li>
					<li>
						<i class="icon ion-images" />
						<div>
							<Link to="/newsfeed-images">Imagens</Link>
						</div>
					</li>
					<li>
						<i class="icon ion-ios-videocam" />
						<div>
							<Link to="/newsfeed-videos">Vídeos</Link>
						</div>
					</li>
				</ul>
				<div id="chat-block">
					<div
						class="title"
						style={{ textAlign: 'center', backgroundColor: '#4ea9d8' }}
					>
						Medalhas
					</div>
					<ul class="online-users list-inline">
						<li>
							<Link to="/newsfeed-messages" title="Linda Lohan">
								<img
									src="https://cdn.icon-icons.com/icons2/1193/PNG/512/1490877821-sport-badges02_82418.png"
									alt="user"
									class="img-responsive profile-photo"
								/>
							</Link>
						</li>
						<li>
							<Link to="/newsfeed-messages" title="Sophia Lee">
								<img
									src="https://cdn.icon-icons.com/icons2/1193/PNG/512/1490877848-sport-badges04_82412.png"
									alt="user"
									class="img-responsive profile-photo"
								/>
							</Link>
						</li>
						<li>
							<Link to="/newsfeed-messages" title="John Doe">
								<img
									src="https://www.fisetoscana.com/obj/image/trofeo.png"
									alt="user"
									class="img-responsive profile-photo"
								/>
							</Link>
						</li>
						<li>
							<Link to="/newsfeed-messages" title="Alexis Clark">
								<img
									src="https://cdn.icon-icons.com/icons2/1193/PNG/512/1490877816-sport-badges12_82411.png"
									alt="user"
									class="img-responsive profile-photo"
								/>
							</Link>
						</li>
						<li>
							<Link to="/newsfeed-messages" title="James Carter">
								<img
									src="https://cdn0.iconfinder.com/data/icons/sport-achievment-badges/128/sport_badges-03-512.png"
									alt="user"
									class="img-responsive profile-photo"
								/>
							</Link>
						</li>
						<li>
							<Link to="/newsfeed-messages" title="Robert Cook">
								<img
									src="https://lh3.googleusercontent.com/QIJOyanjaTLJ5Ku6NGziyI0cDVcY48kRKYBPfLGEed8wlT2MDnB3qqJT8yjEyTdTKyrV"
									alt="user"
									class="img-responsive profile-photo"
								/>
							</Link>
						</li>
						<li>
							<Link to="/newsfeed-messages" title="Richard Bell">
								<img
									src="https://cdn.icon-icons.com/icons2/1193/PNG/512/1490877822-sport-badges08_82413.png"
									alt="user"
									class="img-responsive profile-photo"
								/>
							</Link>
						</li>
					</ul>
				</div>
			</div>
		);
	}
}

const mapStateToProps = function(state) {
	return { state: state };
};

const mapDispatchToProps = function(dispatch) {
	return {
		updateProfile: function(uid, picture, name) {
			return dispatch({ type: 'UPDATE_PROFILE', uid, picture, name });
		}
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ProfileNewsFeed);

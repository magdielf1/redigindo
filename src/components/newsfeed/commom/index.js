import React, { Component } from 'react';
import ProfileNewsFeed from './profile-newsfeed';
import SideBarNewsFeed from './side-bar-newsfeed';

class NewsFeedCommom extends Component {
	render() {
		return (
			<div id="page-contents">
				<div class="container">
					<div class="row">
						<ProfileNewsFeed />
						<div class="col-md-7 content-interior">
							{React.cloneElement(this.props.children, {})}
						</div>
						<SideBarNewsFeed />
					</div>
				</div>
			</div>
		);
	}
}

export default NewsFeedCommom;

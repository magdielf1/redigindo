import React, { Component } from 'react';

class CreatePostNewsFeed extends Component {
	render() {
		return (
			<div class="create-post">
				<div class="row">
					<div class="col-md-7 col-sm-7">
						<div class="form-group">
							<img
								src="http://placehold.it/300x300"
								alt=""
								class="profile-photo-md"
							/>
							<textarea
								name="texts"
								id="exampleTextarea"
								cols="30"
								rows="1"
								class="form-control"
								placeholder="Write what you wish"
							/>
						</div>
					</div>
					<div class="col-md-5 col-sm-5">
						<div class="tools">
							<ul class="publishing-tools list-inline">
								<li>
									<a href="/">
										<i class="ion-compose" />
									</a>
								</li>
								<li>
									<a href="/">
										<i class="ion-images" />
									</a>
								</li>
								<li>
									<a href="/">
										<i class="ion-ios-videocam" />
									</a>
								</li>
								<li>
									<a href="/">
										<i class="ion-map" />
									</a>
								</li>
							</ul>
							<button class="btn btn-primary pull-right">Publish</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default CreatePostNewsFeed;

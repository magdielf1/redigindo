import React, { Component } from 'react';
import { Link, hashHistory } from 'react-router';
import { connect } from 'react-redux';
import firebaseApp from '../../utilidade/firebase/config';

class Navbar extends Component {
	componentWillMount() {
		let _this = this;
		firebaseApp.auth().onAuthStateChanged(function(user) {
			if (user) {
				//console.log(user);
				_this.props.updateProfile(user.uid, user.photoURL, user.displayName);
				_this.props.changeStatus(false);
			} else {
				_this.props.changeStatus(false);
			}
		});
	}
	render() {
		return (
			<React.Fragment>
				<header id="header">
					<nav
						className="navbar navbar-default navbar-fixed-top menu"
						style={{
							background: '#231f2014'
						}}
					>
						<div className="container">
							<div className="navbar-header">
								<button
									type="button"
									className="navbar-toggle collapsed"
									data-toggle="collapse"
									data-target="/bs-example-navbar-collapse-1"
									aria-expanded="false"
								>
									<span className="sr-only">Toggle navigation</span>
									<span className="icon-bar" />
									<span className="icon-bar" />
									<span className="icon-bar" />
								</button>
								<Link
									className="navbar-brand"
									style={{ padding: '20px' }}
									to="/"
								>
									<img
										src="images/logo2.png"
										alt="logo"
										style={{
											width: 140,
											position: 'relative',
											top: '-16px'
										}}
									/>
								</Link>
							</div>

							<div
								className="collapse navbar-collapse"
								id="bs-example-navbar-collapse-1"
							>
								<form className="navbar-form navbar-right hidden-sm">
									<div className="form-group">
										<i
											className="icon ion-android-search"
											style={{ color: '#6f6666' }}
										/>
										<input
											type="text"
											className="form-control"
											placeholder="Pesquise postagens ou pessoas"
											style={{
												color: '#6f6666',
												background: 'rgba(255, 255, 255, 0.92)'
											}}
										/>
									</div>
								</form>
							</div>
						</div>
					</nav>
				</header>
			</React.Fragment>
		);
	}
}

const mapStateToProps = function(state) {
	return { state: state };
};

const mapDispatchToProps = function(dispatch) {
	return {
		updateProfile: function(uid, picture, name) {
			return dispatch({ type: 'UPDATE_PROFILE', uid, picture, name });
		},
		changeStatus: function(status) {
			return dispatch({ type: 'CHANGE_STATUS', status });
		}
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Navbar);

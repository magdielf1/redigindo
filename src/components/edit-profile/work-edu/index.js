import React, { Component } from 'react';

class EditProfileWorkEdu extends Component {
    render() {
        return (
            <div className="col-md-7">

                {/* <!-- Edit Work and Education
================================================= --> */}
                <div className="edit-profile-container">
                    <div className="block-title">
                        <h4 className="grey"><i className="icon ion-ios-book-outline"></i>My education</h4>
                        <div className="line"></div>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate</p>
                        <div className="line"></div>
                    </div>
                    <div className="edit-block">
                        <form name="education" id="education" className="form-inline">
                            <div className="row">
                                <div className="form-group col-xs-12">
                                    <label for="school">My university</label>
                                    <input id="school" className="form-control input-group-lg" type="text" name="school" title="Enter School" placeholder="My School" value="Harvard Unversity" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-6">
                                    <label for="date-from">From</label>
                                    <input id="date-from" className="form-control input-group-lg" type="text" name="date" title="Enter a Date" placeholder="from" value="2012" />
                                </div>
                                <div className="form-group col-xs-6">
                                    <label for="date-to" className="">To</label>
                                    <input id="date-to" className="form-control input-group-lg" type="text" name="date" title="Enter a Date" placeholder="to" value="2016" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-12">
                                    <label for="edu-description">Description</label>
                                    <textarea id="edu-description" name="description" className="form-control" placeholder="Some texts about my education" rows="4" cols="400">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate</textarea>
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-12">
                                    <label for="graduate">Graduated?:-</label>
                                    <input id="graduate" type="checkbox" name="graduate" value="graduate" checked /> Yes!!
</div>
                            </div>
                            <button className="btn btn-primary">Save Changes</button>
                        </form>
                    </div>
                    <div className="block-title">
                        <h4 className="grey"><i className="icon ion-ios-briefcase-outline"></i>Work Experiences</h4>
                        <div className="line"></div>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate</p>
                        <div className="line"></div>
                    </div>
                    <div className="edit-block">
                        <form name="work" id="work" className="form-inline">
                            <div className="row">
                                <div className="form-group col-xs-12">
                                    <label for="company">Company</label>
                                    <input id="company" className="form-control input-group-lg" type="text" name="company" title="Enter Company" placeholder="Company name" value="Envato Inc" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-12">
                                    <label for="designation">Designation</label>
                                    <input id="designation" className="form-control input-group-lg" type="text" name="designation" title="Enter designation" placeholder="designation name" value="Exclusive Author" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-6">
                                    <label for="from-date">From</label>
                                    <input id="from-date" className="form-control input-group-lg" type="text" name="date" title="Enter a Date" placeholder="from" value="2016" />
                                </div>
                                <div className="form-group col-xs-6">
                                    <label for="to-date" className="">To</label>
                                    <input id="to-date" className="form-control input-group-lg" type="text" name="date" title="Enter a Date" placeholder="to" value="Present" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-12">
                                    <label for="work-city">City/Town</label>
                                    <input id="work-city" className="form-control input-group-lg" type="text" name="city" title="Enter city" placeholder="Your city" value="Melbourne" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-12">
                                    <label for="work-description">Description</label>
                                    <textarea id="work-description" name="description" className="form-control" placeholder="Some texts about my work" rows="4" cols="400">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate</textarea>
                                </div>
                            </div>
                            <button className="btn btn-primary">Save Changes</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditProfileWorkEdu;

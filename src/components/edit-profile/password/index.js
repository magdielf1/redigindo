import React, { Component } from 'react';

class EditProfilePassword extends Component {
    render() {
        return (
            <div className="col-md-7">

                {/* <!-- Change Password
              ================================================= --> */}
                <div className="edit-profile-container">
                    <div className="block-title">
                        <h4 className="grey"><i className="icon ion-ios-locked-outline"></i>Change Password</h4>
                        <div className="line"></div>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate</p>
                        <div className="line"></div>
                    </div>
                    <div className="edit-block">
                        <form name="update-pass" id="education" className="form-inline">
                            <div className="row">
                                <div className="form-group col-xs-12">
                                    <label for="my-password">Old password</label>
                                    <input id="my-password" className="form-control input-group-lg" type="password" name="password" title="Enter password" placeholder="Old password" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-6">
                                    <label>New password</label>
                                    <input className="form-control input-group-lg" type="password" name="password" title="Enter password" placeholder="New password" />
                                </div>
                                <div className="form-group col-xs-6">
                                    <label>Confirm password</label>
                                    <input className="form-control input-group-lg" type="password" name="password" title="Enter password" placeholder="Confirm password" />
                                </div>
                            </div>
                            <p><a href="/">Forgot Password?</a></p>
                            <button className="btn btn-primary">Update Password</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditProfilePassword;

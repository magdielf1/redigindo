import React, { Component } from 'react';

class EditProfileSettings extends Component {
    render() {
        return (
            <div className="col-md-7">

                {/* <!-- Profile Settings
                                ================================================= --> */}
                <div className="edit-profile-container">
                    <div className="block-title">
                        <h4 className="grey"><i className="icon ion-ios-settings"></i>Account Settings</h4>
                        <div className="line"></div>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate</p>
                        <div className="line"></div>
                    </div>
                    <div className="edit-block">
                        <div className="settings-block">
                            <div className="row">
                                <div className="col-sm-9">
                                    <div className="switch-description">
                                        <div><strong>Enable follow me</strong></div>
                                        <p>Enable this if you want people to follow you</p>
                                    </div>
                                </div>
                                <div className="col-sm-3">
                                    <div className="toggle-switch">
                                        <label className="switch">
                                            <input type="checkbox" checked />
                                            <span className="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="line"></div>
                        <div className="settings-block">
                            <div className="row">
                                <div className="col-sm-9">
                                    <div className="switch-description">
                                        <div><strong>Send me notifications</strong></div>
                                        <p>Send me notification emails my friends like, share or message me</p>
                                    </div>
                                </div>
                                <div className="col-sm-3">
                                    <div className="toggle-switch">
                                        <label className="switch">
                                            <input type="checkbox" checked />
                                            <span className="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="line"></div>
                        <div className="settings-block">
                            <div className="row">
                                <div className="col-sm-9">
                                    <div className="switch-description">
                                        <div><strong>Text messages</strong></div>
                                        <p>Send me messages to my cell phone</p>
                                    </div>
                                </div>
                                <div className="col-sm-3">
                                    <div className="toggle-switch">
                                        <label className="switch">
                                            <input type="checkbox" />
                                            <span className="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="line"></div>
                        <div className="settings-block">
                            <div className="row">
                                <div className="col-sm-9">
                                    <div className="switch-description">
                                        <div><strong>Enable tagging</strong></div>
                                        <p>Enable my friends to tag me on their posts</p>
                                    </div>
                                </div>
                                <div className="col-sm-3">
                                    <div className="toggle-switch">
                                        <label className="switch">
                                            <input type="checkbox" checked />
                                            <span className="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="line"></div>
                        <div className="settings-block">
                            <div className="row">
                                <div className="col-sm-9">
                                    <div className="switch-description">
                                        <div><strong>Enable sound</strong></div>
                                        <p>You'll hear notification sound when someone sends you a private message</p>
                                    </div>
                                </div>
                                <div className="col-sm-3">
                                    <div className="toggle-switch">
                                        <label className="switch">
                                            <input type="checkbox" checked />
                                            <span className="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditProfileSettings;

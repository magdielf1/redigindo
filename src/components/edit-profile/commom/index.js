import React, { Component } from 'react';
import TimelineCoverEditProfile from './timeline-cover';
import TimelineActivitySidebar from './timeline-activity-sidebar';
import ProfileMenu from './profile-menu';

class EditProfileCommom extends Component {
    render() {
        return (
            <div className="container">
                <div className="timeline">
                    <TimelineCoverEditProfile />
                    <div id="page-contents">
                        <div className="row">
                            <ProfileMenu location={this.props.location} />
                            {React.cloneElement(this.props.children, {})}
                            <TimelineActivitySidebar />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditProfileCommom;

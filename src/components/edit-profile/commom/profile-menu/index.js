import React, { Component } from 'react';
import { Link } from 'react-router/lib';

class ProfileMenu extends Component {

    componentWillMount(){
      
    }

    isActive(option) {
		let { pathname } = this.props.location;

        if(option === pathname){
            return true;
        }
        return false;
	}

    render() {
        return (
            <div className="col-md-3">
                <ul className="edit-menu">
                    <li className={this.isActive('/edit-profile-basic') ? 'active' : ''}><i className="icon ion-ios-information-outline"></i><Link to="/edit-profile-basic">Basic Information</Link></li>
                    <li className={this.isActive('/edit-profile-work-edu') ? 'active' : ''}><i className="icon ion-ios-briefcase-outline"></i><Link to="/edit-profile-work-edu">Education and Work</Link></li>
                    <li className={this.isActive('/edit-profile-interests') ? 'active' : ''}><i className="icon ion-ios-heart-outline"></i><Link to="/edit-profile-interests">My Interests</Link></li>
                    <li className={this.isActive('/edit-profile-settings') ? 'active' : ''}><i className="icon ion-ios-settings"></i><Link to="/edit-profile-settings">Account Settings</Link></li>
                    <li className={this.isActive('/edit-profile-password') ? 'active' : ''}><i className="icon ion-ios-locked-outline"></i><Link to="/edit-profile-password">Change Password</Link></li>
                </ul>
            </div>
        );
    }
}

export default ProfileMenu;



import React, { Component } from 'react';

class EditProfileInterests extends Component {
	render() {
		return (
            <div className="col-md-7">

              {/* <!-- Edit Interests
              ================================================= --> */}
              <div className="edit-profile-container">
                <div className="block-title">
                  <h4 className="grey"><i className="icon ion-ios-heart-outline"></i>My Interests</h4>
                  <div className="line"></div>
                  <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate</p>
                  <div className="line"></div>
                </div>
                <div className="edit-block">
                  <ul className="list-inline interests">
                  	<li><a href=""><i className="icon ion-android-bicycle"></i> Bycicle</a></li>
                  	<li><a href=""><i className="icon ion-ios-camera"></i> Photgraphy</a></li>
                  	<li><a href=""><i className="icon ion-android-cart"></i> Shopping</a></li>
                  	<li><a href=""><i className="icon ion-android-plane"></i> Traveling</a></li>
                  	<li><a href=""><i className="icon ion-android-restaurant"></i> Eating</a></li>
                  </ul>
                  <div className="line"></div>
                  <div className="row">
                    <p className="custom-label"><strong>Add interests</strong></p>
                    <div className="form-group col-sm-8">
                      <input id="add-interest" className="form-control input-group-lg" type="text" name="interest" title="Choose Interest" placeholder="Interests. For example, photography"/>
                    </div>
                    <div className="form-group col-sm-4">
                      <button className="btn btn-primary">Add</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        );
	}
}

export default EditProfileInterests;

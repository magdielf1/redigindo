import React, { Component } from 'react';
import TimelineCover from './timeline-cover';
import SideBarTimeline from './sidebar';

class TimelineCommom extends Component {
    render() {
        return (
            <div class="container">

                <div class="timeline">
                    <TimelineCover />
                    <div id="page-contents">
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-7">

                                {React.cloneElement(this.props.children, {})}
                                
                            </div>
                            <SideBarTimeline/>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default TimelineCommom;

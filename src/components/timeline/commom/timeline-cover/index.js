import React, { Component } from 'react';
import { Link } from 'react-router/lib';    

class TimelineCover extends Component {
    render() {
        return (
            <div class="timeline-cover">

                <div class="timeline-nav-bar hidden-sm hidden-xs">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="profile-info">
                                <img src="http://placehold.it/300x300" alt="" class="img-responsive profile-photo" />
                                <h3>Sarah Cruiz</h3>
                                <p class="text-muted">Creative Director</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <ul class="list-inline profile-menu">
                                <li><Link to="/timeline">Timeline</Link></li>
                                <li><Link to="/timeline-about">About</Link></li>
                                <li><Link to="/timeline-album">Album</Link></li>
                                <li><Link to="/timeline-friends">Friends</Link></li>
                            </ul>
                            <ul class="follow-me list-inline">
                                <li>1,299 people following her</li>
                                <li><button class="btn-primary">Add Friend</button></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="navbar-mobile hidden-lg hidden-md">
                    <div class="profile-info">
                        <img src="http://placehold.it/300x300" alt="" class="img-responsive profile-photo" />
                        <h4>Sarah Cruiz</h4>
                        <p class="text-muted">Creative Director</p>
                    </div>
                    <div class="mobile-menu">
                        <ul class="list-inline">
                            <li><a href="timline.html" class="active">Timeline</a></li>
                            <li><a href="timeline-about.html">About</a></li>
                            <li><a href="timeline-album.html">Album</a></li>
                            <li><a href="timeline-friends.html">Friends</a></li>
                        </ul>
                        <button class="btn-primary">Add Friend</button>
                    </div>
                </div>

            </div>
        );
    }
}

export default TimelineCover;

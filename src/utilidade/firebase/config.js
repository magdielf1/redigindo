import firebase from 'firebase';

const config = {
	apiKey: 'AIzaSyAjq4Yf7iNXmuaMbCt77UK1JlxFE5RsMls',
	authDomain: 'redon-214111.firebaseapp.com',
	databaseURL: 'https://redon-214111.firebaseio.com',
	projectId: 'redon-214111',
	storageBucket: 'redon-214111.appspot.com',
	messagingSenderId: '121586440077'
};

const firebaseApp = firebase.initializeApp(config);

export default firebaseApp;

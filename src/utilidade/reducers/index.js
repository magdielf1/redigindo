export const UPDATE_PROFILE = 'UPDATE_PROFILE',
	CHANGE_STATUS = 'CHANGE_STATUS';

const stateREDON = {
	profile: {
		userUID: null,
		userPicture: null,
		userName: null
	},
	userStatus: {
		loading: true,
		redacaoAtivada: false
	}
};

const reducer = function(state = stateREDON, action) {
	const { type, uid, picture, name, status } = action;
	switch (type) {
		case UPDATE_PROFILE:
			return {
				...state,
				profile: {
					...state.profile,
					userUID: uid,
					userPicture: picture,
					userName: name
				}
			};
		case CHANGE_STATUS:
			return {
				...state,
				userStatus: {
					...state.userStatus,
					loading: status
				}
			};
		default:
			return state;
	}
};

export default reducer;
